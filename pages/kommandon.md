---
title: Lär känna kommandotolken
slug: kommandon
date: 2020-10-11
---

När vi har loggat in på vår Raspberry Pi, möts vi av en _promp_ som väntar på att vi ska skriva in ett kommando. Det kan se ut så här:

`pi@raspberry:~$`

Vad som egentligen har hänt är att efter inloggningen så har ett program startats. Programmet heter _Bash_ och är en kommandotolk, alltså ett program som kan tolka kommandon som vi skriver, och ge systemet instruktioner.

## `pwd`

Prova att skriva `pwd` och tryck på _enter_. Det utskriften blir `/home/pi`. `pwd` står för _Print Working Directory_ (skriv ut arbetskatalog) och visar vilken som är den aktuella katalogen, vilken katalog vi "står" i. I en skrivbordsmiljö, liksom i Windows eller MacOS, visas en katalog som en mapp, som i sin tur kan innehålla filer (dokument, program med mera) och andra mappar. När du ser orden mapp, folder, directory eller katalog, så avses vanligen samma sak.

## Prompten

Prompten kan se lite kryptisk ut, men det går att utläsa några saker från prompten: före snabel-a:et står namnet på den användare som vi är inloggad som. Efter snabel-a:et står namnet på systemet vi är inloggad på. Sedan följer ett kolon, och efter det står vilken katalog som vi står i, alltså som är aktiv. Prompten kan alltså beskrivas som `[användarnamn]@[systemnamn]:[arbetskatalog]$`.

## `ls`

`ls` står för _list_ och listar de filer och kataloger som finns i den aktuella katalogen (arbetskatalogen). Du kan vanligtvis lägga till options till ett kommando. Prova att skriva följande varianter:
* `ls -a`
* `ls -l`
* `ls -la`
* `ls -hal`

## `mkdir`

`mkdir` står för _make directory_ som betyder skapa katalog. Prova att skriva `mkdir test`. Nu skapas en katalog som heter _test_.

## `cd`

`cd` står för _change directory_ som betyder byt katalog, det vill säga, ändra den aktuella katalogen, den katalog som du "står" i.

## `cat`

## `more` och `less`

## `grep`

## Att styra om utskriften

Prova att skriva `ls -l /etc/`. Det betyder: lista alla filer som finns i katalogen /etc/. Hann du läsa allt? Troligen inte. Prova nu att skriva `ls -l /etc/ | more`. Det betyder att istället för att utskriften visas på skärmen, så skickar pipe-tecknet, `|`, utskriften till ett annat program, i detta fall programmet more, som delar upp utskriften i skärmsidor, så att vi hinner läsa den.

## Få hjälp

Nästa alla kommandon och program tillåter att man anropar dem med optionen `--help`. Då skrivs en hjälptext ut.

### `man`

`man` står för manual. Skriv `man ls`, så visas en komplett manual för kommandot ls. Bläddra uppåt och nedåt med piltangenterna. Avsluta med q.
