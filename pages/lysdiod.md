---
title: Programmera din Raspberry Pi
slug: lysdiod
date: 2020-10-04
---

Till sin  Raspberry Pi kan koppla olika inmatnings- och utmatnings enheter. Exempel på olika inmatningsenheter:

- Mus och Tangentbord
- Mikrofon
- Sensorer, t.ex. rörelse
- Knappar

Exempel på utmatningsenheter:

- Skärm
- Högtalare
- Lysdioder/Lampor

Men Raspberry Pi är det riktigt enkelt att med hjälp av de här olike enheterna (både inmatninga och utmatning) få saker att hända. Der finns massor av olika projekt på nätet där man kan få inspiration och ideer. 

Nu ska vi testa på att koppla in några enheter till Raspberry Pi.

## Koppla
![Add / Remove Software](../images/koppla/add-remove-software.png)

## Koppla in en lysdiod
Nu ska vi testa om det går att tända en lysdiod, genom att programmera vår Rasberry Pi. **Se först till att Rasberry Pi är avstängd.**

Först ska vi koppla in lysdioden. Men alra först ska vi koppla in en Extension Board

![Add / Remove Software](../images/koppla/diod0.jpg "Extension Board")

Här är Raspberry Pi inkopplad till en extension board som sitter på en Breadboard. Här är en närbild på vår lysdiod som vi vill få att lysa på något sätt.


![Add / Remove Software](../images/koppla/diod5.jpg "Breadboard")

För att få den att lysa behöver vi leda ström genom den. 

På bilden här under har vi markerat alla "Pins" med vita, **och 2 röda** punkter. Kan du se de röda punkterna?

![Add / Remove Software](../images/koppla/diod5-1_LI.jpg "Breadboard")


Man kan programmera Rasberry Pi att slå på och av strömmen på varje sån här "Pin". Vi kommer använda oss av två Pins
- GND, som betyder ground eller jord på svenska.
- GPIO18.


På bilden har vi kopplat in jord (GND) från Raspberry Pi till Breadboardets kord (- i blått).

![Add / Remove Software](../images/koppla/diod5_LI.jpg "Breadboard")

Strömmen ska passera igenom lysdioden som de gröna linjerna markerar.

Vi kopplar in GND och GPIO18, så här:

![Add / Remove Software](../images/koppla/diod3.jpg "Breadboard")

Om vi slår på strömmen på GPIO18, och leder den (röd kabel) genom lysdioden, tillbaka till GND (svart kabel).


## Programmera

Vi behöver sätta upp vår utvecklingsmiljö, genom att installera Scratch 3. Antingen via 

![Add / Remove Software](../images/koppla/add-remove-software.png "Add / Remove Software")


eller genom att köra den här kommandona:
`sudo apt-get update`
`sudo apt-get install scratch3`

Nu ska vi sätta igång och programmera. Öppna Scratch 3.
![Open Scratch 3](../images/koppla/open-scratch.png "Open Scratch 3")

När du har öppnat Scratch, lägg till tillägget "GPIO", så här.
![Open Scratch 3](../images/koppla/add-GPIO.png "Lägg till tillägget GPIO")

Gå tillbaka och nu kan vi äntligen starta att programmera vår lysdiod. Lägg till det här programet i Scratch.

![Program](../images/koppla/program.jpg "Program in Scratch to light the diod")


#Koppla in en knapp
Man kan också få Scratch att lyssna på när man trycker på en knapp.

Koppla in en knapp så här:
![Knapp](../images/koppla/button.png "Sätt dit en knapp på kopplingsbrädan.")

Nu ska vi skriva ett program som får saker att hända på scenen i Scratch när man trycker på knappen:

## Lägg till ett tillägg i Scratch
Klicka på Tilläggsmmenyn längst ner till vänster.
![Tillägg](../images/koppla/extension%20for%20button.png "Sätt dit en knapp på kopplingsbrädan.")


![Tillägg](../images/koppla/extension%20for%20button%202.png "Sätt dit en knapp på kopplingsbrädan.")


Så...nu ska vi sätta igång och programmera knappen. Lägg till det här programmet i Scratch.



![Tillägg](../images/koppla/program%20for%20listen%20to%20a%20button.png "Lyssna på en knapptryckning.")
