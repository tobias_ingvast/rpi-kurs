---
title: Koppla in omvärlden
slug: koppla-in-omvarlden
date: 2020-09-22
---

Du kan enkelt koppla olika inmatnings- och utmatnings enheter till din Raspberry Pi. Här är några olika exempel på olika inmatningsenheter:

- Mus och Tangentbord
- Mikrofon
- Sensorer, t.ex. rörelse, temperatur
- Knappar

Här är några exempel på utmatningsenheter:

- Skärm
- Högtalare
- Lysdioder/Lampor

Men Raspberry Pi är det riktigt enkelt att med hjälp av de här olika enheterna (både inmatninga och utmatning) få saker att hända. Det finns dessutom massor av olika projekt på nätet där man kan få inspiration och ideer. 

Nu ska vi testa på att koppla in några enheter till Raspberry Pi!