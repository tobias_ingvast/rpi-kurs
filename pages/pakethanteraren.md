---
title: Pakethanteraren
slug: pakethanteraren
---

Om du klickar på hallonet uppe till vänster, öppnas menyn. Därifrån kommer man åt de program som finns och som är anpassade för skrivbordsmiljön, det vill säga, som har ett grafiskt gränssnitt. 

Men vi ser inte så många program. Hade vi valt den fullständiga versionen av Raspberry Pi OS, hade det varit massor av spännande program här. Vi ska stället välja några själva att installera. Här är några förslag, men utforska gärna själv vad som finns.

Klicka på hallonmenyn och välj _inställningar_, sen `lägg till/ta bort program`. Nu körs programmet _pi-package_ som vi installerade tidigare. Du kan dels leta program i de olika kategorierna, dels söka efter program vid förstoringsglaset. 

Här är några förslag du kan välja bland:

* Du vill nog ha en webbläsare. Sök efter _Firefox_ eller _Chromium_. 
* Testa _libreoffice_, som innehåller kontorsprogram liknande Word, Excel och Powerpoint.
* _imp_ är ett fotoredigeringsprogram, ungefär som Photoshop.
* _Inkscape_ är ett ritprogram, liknande Illustrator.
* Under kategorin _Programmering_ hittar du bland mycket annat _Scratch_. Installera det, för det ska vi använda i nästa del av kursen.
* Testa att installera något spel (men bli inte sittande för länge med spelet ;-) )





