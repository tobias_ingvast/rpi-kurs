---
title: Lär känna din Raspberry Pi
slug: lar-kanna
date: 2020-10-11
---

I det här kapitlet ska vi lära oss lite om Raspberry Pi:s terminal. Där skriver vi textkommandon för att få datorn att göra det vi vill.

Vi ska också titta på hur man installerar program (även kallat _paket_) från terminalen med hjälp av paktehanteraren **apt**.

Fortsätt nu till [Några kommandon](https://prograde.se/rpi/kommandon/index.html)

