---
title: Koppla ihop din Raspberry Pi
slug: koppla-ihop
date: 2020-09-22
---

Förutom själva Raspberry Pi:en behöver du följande:

1. Ditt SD-kort med operativsystemet
2. Ett tangentbord med USB-kontakt
3. En skärm med HDMI-anslutning
4. En strömkälla med Micro USB-kontakt, till exempel en mobilladdare med 5 volts (V) spänning. Men observera att den behöver klara minst 2,5 ampere (A). Om du har en Raspberry Pi 4 måste laddaren klara 3 ampere.

Låt oss ta en titt på Raspberry Pi och utforska dess delar, för att se var vi ska koppla in allt:

![Raspberry Pi 3+ model B](/images/raspberry_pi3_marks.png "Raspberry Pi 3+ model b")

1. Sätt in SD-kortet i Micro SD-kortplatsen (den sitter på undersidan av kretskortet)
2. Koppla in tangentbordet i någon av USB-portarna
3. Koppla in skärmen i HDMI-porten
4. Koppla in strömmen

När du kopplat in allt, startar systemet. En färggrann ruta bör visas på skärmen, och därefter en massa rader text. Grattis! Du är igång! Gå nu vidare till [Installera operativsystemet](https://prograde.se/rpi/pages/installera/index.html).
